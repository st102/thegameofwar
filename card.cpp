#include "card.hpp"

#include <iostream>

/// Return the suit shifted into its representation.
static inline unsigned
to_bits(Suit s)
{
    return static_cast<unsigned>(s) << 4;
}

/// Returns the rank as its representation.
static inline unsigned
to_bits(Rank r)
{
    return static_cast<unsigned>(r);
}

static inline Suit
to_suit(unsigned n)
{
    return static_cast<Suit>(n >> 4);
}

static inline Rank
to_rank(unsigned n)
{
    return static_cast<Rank>(n & 0xf);
}

Card::Card(Rank r, Suit s)
: m_value(to_bits(s) | to_bits(r))
{
}

Rank
Card::get_rank() const
{
    return to_rank(m_value);
}

Suit
Card::get_suit() const
{
    return to_suit(m_value);
}

std::ostream&
operator<<(std::ostream& os, Rank r)
{
    switch (r) {
        case Ace: return os << "A";
        case Two: return os << "2";
        case Three: return os << "3";
        case Four: return os << "4";
        case Five: return os << "5";
        case Six: return os << "6";
        case Seven: return os << "7";
        case Eight: return os << "8";
        case Nine: return os << "9";
        case Ten: return os << "T";
        case Jack: return os << "J";
        case Queen: return os << "Q";
        case King: return os << "K";
    }
}

std::ostream&
operator<<(std::ostream& os, Suit s)
{
    switch (s)
    {
        case Spades: return os << "\u2660";
        case Clubs: return os << "\u2663";
        case Diamonds: return os << "\u2662";
        case Hearts: return os << "\u2661";
    }
}

bool
operator==(Card a, Card b)
{
    return a.get_raw_value() == b.get_raw_value();
}

bool
operator!=(Card a, Card b)
{
    return !(a == b);
}


bool
operator<(Card a, Card b)
{
    return a.get_raw_value() < b.get_raw_value();
}

bool
operator>(Card a, Card b)
{
    return b < a;
}

bool
operator<=(Card a, Card b)
{
    return !(b < a);
}

bool
operator>=(Card a, Card b)
{
    return !(a < b);
}

std::ostream&
operator<<(std::ostream& os, Card c)
{
    return os << c.get_rank() << c.get_suit();
}

//std::istream&
//operator>>(std::istream& is, Rank& r)
//{
//    char c;
//    is >> c;
//    switch(c)
//    {
//        case 'A':
//            r = Ace;
//            break;
//        case '2':
//            r = Two;
//            break;
//        case '3':
//            r = Three;
//            break;
//
//
//
//        default:
//            //what do we do if the character does not represent a rank (e.g 'H')?
//
//            //Options:
//            // do nothing?
//            // Why shouldn't we do this?
//
//            // we have no way to indicate the input value is value we want.
//            /*
//             Caller has no way to detect (or observe) if the input is valid. May not be ideal (here)
//             */
//
//            // print an error message and retry (REAL BAD)
//            // DO NOT DO THIS
//            //            while (???){
//            //                cout <<  "Try again: ";
//            //                cin >> r;
//            //            }
//            //it is not the responsibility of *this* function to figure out what do to in teh error cases. it is responsibility of the caller.
//            //Implies that we need to communicate error state to the caller
//
//            // Guggest a rant
//
//            //Pick a default value
//            //Not an error condition. We might have multiple aces card of the same rank if we put a default value incase of an error.
//            //We can't tell if he input contained a valid value or not.
//
//            // guess what the user meant?
//
//            // Exit the program ( with error code)
//            // this is little extreme.
//            //Exit implies that your program has run to completion.
//            //The exit code is used to communicate with the progra that runs this program, not to indicate an error within this program.
//
//            //A bit extreme, but not the worst thign you can do.
//
//            std::exit(EXIT_FAILURE);
//
//            // Abort the program
//            //Also a bit extreme. BUT gives an opportunity to debug a program.
//            //Use abort to stop the program when a logic error is detected.
//            //This is usefull for debugging but *rarely* production systems.
//
//
//            // Throw an exception
//            //This is not bad.
//            //throw std::runtime_error();
//            //Usually exceptions are thrown for exceptional behaviors (input)
//                //somebody, somewhere has to deal with exceptions. These are often used for "system is going down" kinds of errors. They give the system a chance to respond to a critical failure of some kind.
//
//            // Indicate some error state
//
//            // "return" an error code.
//                //this is a good strategy for validating input user input
//            //Sets the fail bit for the istream
//            //Don't throw expection for input validation
//            //
//            is.setstate(std::ios::failbit);
//
//            //code for main:
//            // Rank r;
//            //std::cin>> r;
//            /*
//
//
//             if (!std::cin)
//             {
//             std::cerr<< "Invalid rank";
//             return EXIT_FAILURE;
//             }
//             */
//
//    }
//    return is;
//}
